C++17 Code Project Files
========================

Current tested with:
- g++-9
- Scons 2.4.1


Building the exercise template with Scons
-----------------------------------------
1.  $ cd Exercise_template/
2.  $ scons
4.  $ ./Debug/AC++11-401
4.  $ scons -c  # To clean


Building an exercise solution (exercise_xx,
where xx is the exercise number) with Scons
------------------------------------------
1.  $ cd Solutions/exercise_xx/
2.  $ scons


Building all exercise solutions with Scons
------------------------------------------
1.  $ cd Solutions
2.  $ scons



