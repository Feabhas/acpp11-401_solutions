#ifndef EVENT_LIST_H
#define EVENT_LIST_H

#include "Event.h"
#include <vector>

class Event_list : private std::vector<Event> {
public:
  using Base = vector<Event>;

  using Base::begin;
  using Base::emplace_back;
  using Base::end;
  using Base::erase;
  using Base::push_back;
  using Base::reserve;
  using Base::value_type;
};

#endif