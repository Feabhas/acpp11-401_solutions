#ifndef EVENT_LIST_H
#define EVENT_LIST_H

#include "Event.h"
#include <list>

class Event_list : private std::list<Event> {
public:
  using Base = list<Event>;

  using Base::value_type;

  using Base::begin;
  using Base::emplace_back;
  using Base::empty;
  using Base::end;
};

#endif