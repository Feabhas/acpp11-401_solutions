#ifndef PIPE_H
#define PIPE_H

#include "Event_list.h"
#include <optional>
class Pipe {
public:
  using element_type = Event_list;

  void                        push(const element_type& in_val);
  std::optional<element_type> pull();

private:
  element_type element{};
};

#endif