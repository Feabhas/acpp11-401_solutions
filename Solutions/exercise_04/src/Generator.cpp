#include "Generator.h"
#include "Event_list.h"
#include "Pipe.h"
#include <cstdlib>
#include <functional>
#include <iostream>

using namespace std;

void connect(Generator& gen, Pipe& pipe)
{
  gen.output = &pipe;
}

namespace
{
  inline Alarm random_alarm() { return static_cast<Alarm>((rand() % 3) + 1); }

  char const* warning_text[]{
    "It's all gone wrong!",   "Panic!",      "Run away!",
    "Danger, Will Robinson!", "Aaaaaaargh!", "Oh no!"
  };

  inline char const* random_text() { return warning_text[rand() % 6]; }
} // namespace

void Generator::execute()
{
  cout << "GENERATOR: ------------------------" << endl;

  auto       num_events = rand() % 9 + 1;
  Event_list events{};

  cout << "Generating " << num_events << " event"
       << (num_events == 1 ? "" : "s") << endl;

  for (int i = 0; i < num_events; ++i) {
    events.emplace_back(random_alarm(), random_text());
  }

  // Move the Event_list into the pipe.
  // events is now an x-value
  //
  output->push(move(events));

  cout << endl;
}