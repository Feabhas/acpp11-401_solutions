#ifndef THREADSAFE_BUFFER_H
#define THREADSAFE_BUFFER_H

#include "Buffer.h"
#include <condition_variable>
#include <mutex>

template<typename T, size_t sz>
class Threadsafe_buffer : private Buffer<T, sz> {
public:
  using value_type = T;

  template<typename U>
  void   add(U&& in);
  T      get();
  bool   is_empty() const;
  size_t size() const;

private:
  using Base = Buffer<T, sz>;

  // mutex must be mutable for
  // use with const member
  // functions
  //
  mutable std::mutex      mtx{};
  std::condition_variable has_data{};
  std::condition_variable has_space{};
};

template<typename T, size_t sz>
template<typename U>
void Threadsafe_buffer<T, sz>::add(U&& in)
{
  std::unique_lock<std::mutex> lock{ mtx };

  while (Base::size() == sz) {
    has_space.wait(lock);
  }

  Base::add(std::forward<U>(in));
  has_data.notify_one();
}

template<typename T, size_t sz>
T Threadsafe_buffer<T, sz>::get()
{
  std::unique_lock<std::mutex> lock{ mtx };

  while (Base::is_empty()) {
    has_data.wait(lock);
  }

  auto out = Base::get();
  has_space.notify_one();
  return out;
}

template<typename T, size_t sz>
bool Threadsafe_buffer<T, sz>::is_empty() const
{
  std::lock_guard<std::mutex> lock{ mtx };
  return Base::is_empty();
}

template<typename T, size_t sz>
size_t Threadsafe_buffer<T, sz>::size() const
{
  std::lock_guard<std::mutex> lock{ mtx };
  return Base::size();
}

#endif