#include "ID_filter.h"
#include "Pipe.h"
#include <algorithm>
#include <iostream>

using namespace std;

void connect(ID_filter& filter, Pipe& in, Pipe& out)
{
  filter.input  = &in;
  filter.output = &out;
}

void ID_filter::execute()
{
  if (auto events = input->pull()) {
    cout << "ID_FILTER: ------------------------" << endl;

    auto event_list =
      std::move(*events); // create element_type object (unique_ptr)
    auto initial_size = event_list->size();

    auto it = remove_if(
      begin(*event_list), end(*event_list), [this](const Event& event) {
        return event.type() == filter_value;
      });

    event_list->erase(it, end(*event_list));

    auto event_removed = initial_size - event_list->size();

    cout << "Removed " << event_removed << " event";
    cout << (event_removed == 1 ? "" : "s") << endl;

    output->push(move(event_list));

    cout << endl;
  }
}