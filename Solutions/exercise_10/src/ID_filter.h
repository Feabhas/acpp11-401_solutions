#ifndef ID_FILTER
#define ID_FILTER

#include "Event.h"
#include "Filter.h"

class Pipe;

class ID_filter : public Filter {
public:
  ID_filter(Alarm remove_this) : filter_value{ remove_this } {}

  void execute() override;

private:
  Alarm filter_value{ Alarm::unknown };

  Pipe*       input{};
  Pipe*       output{};
  friend void connect(ID_filter& filter, Pipe& in, Pipe& out);
};

#endif