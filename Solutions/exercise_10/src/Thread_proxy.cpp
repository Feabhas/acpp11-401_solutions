#include "Thread_proxy.h"
#include <iostream>
#include <pthread.h>

using namespace std;

Thread::~Thread()
{
  cout << "Terminating thread " << name << endl;

  if (joinable()) {
    cout << "WARNING: Killing joinable thread" << endl;
    detach();
  }

  auto handle = native_handle();
  pthread_cancel(handle);
}