
#include "Display.h"
#include "Generator.h"
#include "ID_filter.h"
#include "Pipe.h"
#include "Pipeline.h"
#include "Thread_proxy.h"
#include <chrono>
#include <thread>

using namespace std;
using namespace chrono_literals;

int main()
{
  Generator generator{};
  Display   display{};
  ID_filter filter{ Alarm::advisory };

  Pipe pipe1{};
  Pipe pipe2{};

  connect(generator, pipe1);
  connect(filter, pipe1, pipe2);
  connect(display, pipe2);

  auto run_n_times = [](int num_times, Filter& filter) {
    for (int i{ 0 }; i < num_times; ++i) {
      filter.execute();
      this_thread::sleep_for(1000ms);
    }
  };

  auto run_forever = [](Filter& filter) {
    while (true) {
      filter.execute();
      this_thread::yield();
    }
  };

  Thread gen_thread{ "Generator", run_n_times, 5, ref(generator) };
  Thread filter_thread{ "ID Filter", run_forever, ref(filter) };
  Thread display_thread{ "Display", run_forever, ref(display) };

  // These threads will be terminated when
  // they go out of scope at the end of
  // main()
  //
  filter_thread.detach();
  display_thread.detach();

  // Wait for the generator to
  // finish
  //
  gen_thread.join();
}