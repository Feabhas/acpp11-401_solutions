#include "Display.h"
#include "Generator.h"
#include "Pipe.h"
#include "Pipeline.h"

int main()
{
  Generator generator{};
  Display   display{};
  Pipe      pipe{};

  connect(generator, pipe);
  connect(display, pipe);

  Pipeline pipeline{};
  pipeline.add(generator);
  pipeline.add(display);
  pipeline.run();
}