#include "Pipe.h"
#include <functional>

using namespace std;

std::optional<Pipe::element_type> Pipe::pull()
{
  if (elements.is_empty()) return std::nullopt;
  return elements.get();
}
