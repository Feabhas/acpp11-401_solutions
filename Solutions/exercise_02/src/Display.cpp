#include "Display.h"
#include "Event.h"
#include "Pipe.h"
#include <iostream>

using namespace std;

void connect(Display& disp, Pipe& pipe)
{
  disp.input = &pipe;
}

void Display::execute()
{
  cout << "DISPLAY  : ------------------------" << endl;

  if (input) {
    if (auto event = input->pull()) {
      cout << "Received event: " << event->as_string() << '\n';
      cout << endl;
    }
  }
  else {
    cerr << "DISPLAY  : No input pipe connected" << endl;
  }
}
