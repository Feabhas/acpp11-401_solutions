#include "Generator.h"
#include "Event.h"
#include "Pipe.h"
#include <cstdlib>
#include <iostream>

using namespace std;

void connect(Generator& gen, Pipe& pipe)
{
  gen.output = &pipe;
}

namespace
{
  inline Alarm random_alarm() { return static_cast<Alarm>((rand() % 3) + 1); }
} // namespace

void Generator::execute()
{
  cout << "GENERATOR: ------------------------" << endl;

  if (output) {
    Event event{ random_alarm() };
    output->push(event);
    cout << "Pushed event: " << event.as_string() << '\n';
    cout << endl;
  }
  else {
    cerr << "GENERATOR  : No output pipe connected" << endl;
  }
}