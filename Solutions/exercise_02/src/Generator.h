#ifndef GENERATOR_H
#define GENERATOR_H

class Pipe;

class Generator {
public:
  Generator() = default;
  Generator(Pipe& ip) : output{ &ip } {}

  void execute();

private:
  Pipe*       output{};
  friend void connect(Generator& gen, Pipe& pipe);
};

#endif