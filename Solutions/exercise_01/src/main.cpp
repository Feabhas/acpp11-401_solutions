#include "Event.h"
#include <iostream>

using std::cout;
using std::endl;

int main()
{
  Event event1{};
  Event event2{ Alarm::warning };

  cout << event1.as_string() << endl;
  cout << event2.as_string() << endl;
}