#include "Pipeline.h"
#include "Filter.h"

void Pipeline::add(Filter& filter)
{
  filters.push_back(&filter);
}

void Pipeline::run()
{
  for (int i{ 0 }; i < 10; ++i) {
    for (auto& filter : filters) {
      filter->execute();
    }
  }
}