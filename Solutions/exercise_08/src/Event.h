#ifndef EVENT_H
#define EVENT_H

enum class Alarm { unknown, advisory, caution, warning };

class Event {
public:
  Event();
  Event(Alarm init);
  Event(Alarm init, const char* str);

  // Copy and move policy
  //
  ~Event();
  Event(const Event& src);
  Event(Event&&) noexcept;
  Event&      operator=(Event rhs);
  friend void swap(Event& lhs, Event& rhs);

  Alarm       type() const;
  const char* as_string() const;
  const char* what() const;

private:
  Alarm event_type{ Alarm::unknown };
  char* description{};
};

#endif