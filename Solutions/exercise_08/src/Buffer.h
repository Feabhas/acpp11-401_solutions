#ifndef BUFFER_H
#define BUFFER_H

#include <array>
#include <stdexcept>

class Buffer_full : public std::out_of_range {
public:
  Buffer_full() : out_of_range{ "Buffer full" } {}
};

class Buffer_empty : public std::out_of_range {
public:
  Buffer_empty() : out_of_range{ "Buffer empty" } {}
};

template<typename T, size_t sz>
class Buffer {
public:
  using value_type = T;

  template<typename U>
  void   add(U&& in);
  T      get();
  bool   is_empty() const;
  size_t size() const;

private:
  using Container = std::array<T, sz>;
  using Iterator  = typename Container::iterator;

  Container buffer{};
  Iterator  write{ std::begin(buffer) };
  Iterator  read{ std::begin(buffer) };
  size_t    num_items{ 0 };
};

template<typename T, size_t sz>
template<typename U>
void Buffer<T, sz>::add(U&& in)
{
  if (num_items == sz) throw Buffer_full{};

  *write = std::forward<U>(in);
  ++write;
  ++num_items;
  if (write == std::end(buffer)) write = std::begin(buffer);
}

template<typename T, size_t sz>
T Buffer<T, sz>::get()
{
  if (num_items == 0) throw Buffer_empty{};

  Iterator out = read;
  ++read;
  --num_items;
  if (read == std::end(buffer)) read = std::begin(buffer);

  return std::move(*out);
}

template<typename T, size_t sz>
bool Buffer<T, sz>::is_empty() const
{
  return (num_items == 0);
}

template<typename T, size_t sz>
size_t Buffer<T, sz>::size() const
{
  return num_items;
}

#endif