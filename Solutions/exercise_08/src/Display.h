#ifndef DISPLAY_H
#define DISPLAY_H

#include "Filter.h"

class Pipe;

class Display : public Filter {
public:
  void execute() override;

private:
  Pipe*       input{};
  friend void connect(Display& disp, Pipe& pipe);
};

#endif