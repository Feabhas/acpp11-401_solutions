#include "Event.h"
#include <cstring>
#include <functional>

#ifdef TRACE_ENABLED
#include <iostream>
#define TRACE(msg)                                                             \
  std::cout << "Event: " << as_string() << " : " << msg << std::endl
#else
#define TRACE(msg)
#endif

using namespace std;

static const char* strings[]{ "unknown  ",
                              "advisory ",
                              "caution  ",
                              "warning  " };

Event::Event()
{
  TRACE("Default ctor");
}

Event::Event(Alarm init) : event_type{ init }
{
  TRACE("Non-default ctor");
}

Event::Event(Alarm init, const char* str) : event_type{ init }
{
  TRACE("Non-default ctor with string");

  if (str) {
    description = new char[strlen(str) + 1];
    strcpy(description, str);
  }
}

Event::Event(Event&& src) noexcept : Event{}
{
  TRACE("Move ctor");

  swap(*this, src);
}

Event::~Event()
{
  TRACE("Dtor");

  delete[] description;
}

Event::Event(const Event& src) : Event{ src.event_type, src.description }
{
  TRACE("Copy ctor");
}

Event& Event::operator=(Event rhs)
{
  TRACE("Copy assignment");

  swap(*this, rhs);
  return *this;
}

Alarm Event::type() const
{
  return event_type;
}

const char* Event::as_string() const
{
  return strings[static_cast<int>(event_type)];
}

const char* Event::what() const
{
  return (description ? description : "");
}

void swap(Event& lhs, Event& rhs)
{
  using std::swap;
  swap(lhs.event_type, rhs.event_type);
  swap(lhs.description, rhs.description);
}