#ifndef PIPE_H
#define PIPE_H

#include "Buffer.h"
#include "Event_list.h"
#include <functional>
#include <optional>

class Pipe {
public:
  using element_type = Event_list_ptr;

  template<typename T>
  void                        push(T&& in_val);
  std::optional<element_type> pull();

private:
  using Internal_buffer = Buffer<Event_list_ptr, 8>;

  Internal_buffer elements{};
};

template<typename T>
void Pipe::push(T&& in_val)
{
  elements.add(std::forward<T>(in_val));
}

#endif