#include "Display.h"
#include "Event.h"
#include "Pipe.h"
#include <iostream>

using namespace std;

void connect(Display& disp, Pipe& pipe)
{
  disp.input = &pipe;
}

void Display::execute()
{
  cout << "DISPLAY  : ------------------------" << endl;
  if (auto events = input->pull()) {
    for (auto& event : **events) {
      cout << event.as_string() << " : " << event.what() << endl;
    }

    cout << endl;
  }
}
