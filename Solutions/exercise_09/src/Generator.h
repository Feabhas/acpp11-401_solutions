#ifndef GENERATOR_H
#define GENERATOR_H

#include "Filter.h"

class Pipe;

class Generator : public Filter {
public:
  void execute() override;

private:
  Pipe*       output{};
  friend void connect(Generator& gen, Pipe& pipe);
};

#endif