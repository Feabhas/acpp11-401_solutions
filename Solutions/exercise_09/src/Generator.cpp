#include "Generator.h"
#include "Event_list.h"
#include "Pipe.h"
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>

using namespace std;

void connect(Generator& gen, Pipe& pipe)
{
  gen.output = &pipe;
}

namespace
{
  inline Alarm random_alarm() { return static_cast<Alarm>((rand() % 3) + 1); }

  char const* warning_text[]{
    "It's all gone wrong!",   "Panic!",      "Run away!",
    "Danger, Will Robinson!", "Aaaaaaargh!", "Oh no!"
  };

  inline char const* random_text() { return warning_text[rand() % 6]; }

} // namespace

void Generator::execute()
{
  cout << "GENERATOR: ------------------------" << endl;

  auto           num_events = rand() % 9 + 1;
  Event_list_ptr events     = make_unique<Event_list>();

  // To avoid copying during insertion
  //
  events->reserve(num_events);

  cout << "Generating " << num_events << " event"
       << (num_events == 1 ? "" : "s") << endl;

  generate_n(back_inserter(*events), num_events, []() {
    return Event{ random_alarm(), random_text() };
  });

  // Move the Event_list into the pipe.
  // events is now an x-value
  //
  output->push(move(events));

  cout << endl;
}