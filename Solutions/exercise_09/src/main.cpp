#include "Display.h"
#include "Generator.h"
#include "ID_filter.h"
#include "Pipe.h"
#include "Pipeline.h"

int main()
{
  Generator generator{};
  Display   display{};
  ID_filter id_filter{ Alarm::advisory };

  Pipe pipe1{};
  Pipe pipe2{};

  connect(generator, pipe1);
  connect(id_filter, pipe1, pipe2);
  connect(display, pipe2);

  Pipeline pipeline{};
  pipeline.add(generator);
  pipeline.add(id_filter);
  pipeline.add(display);
  pipeline.run();
}