#include "Generator.h"
#include "Event_list.h"
#include "Pipe.h"
#include <cstdlib>
#include <iostream>

using namespace std;

void connect(Generator& gen, Pipe& pipe)
{
  gen.output = &pipe;
}

namespace
{
  inline Alarm random_alarm() { return static_cast<Alarm>((rand() % 3) + 1); }
} // namespace

void Generator::execute()
{
  cout << "GENERATOR: ------------------------" << endl;

  auto       num_events = rand() % 9 + 1;
  Event_list events{};

  cout << "Generating " << num_events << " event"
       << (num_events == 1 ? "" : "s") << endl;

  for (int i{ 0 }; i < num_events; ++i) {
    events.emplace_back(random_alarm());
  }

  output->push(events);

  cout << endl;
}