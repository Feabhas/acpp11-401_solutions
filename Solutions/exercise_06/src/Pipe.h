#ifndef PIPE_H
#define PIPE_H

#include "Event_list.h"
#include <optional>

class Pipe {
public:
  using element_type = Event_list_ptr;

  // unique_ptr doesn't support copy
  //
  // void push(const element_type& in_val);

  void                        push(element_type&& in_val);
  std::optional<element_type> pull();

private:
  element_type element{};
};

#endif