#ifndef PIPELINE_H
#define PIPELINE_H

#include <list>

class Filter;

class Pipeline {
public:
  void add(Filter& filter);
  void run();

private:
  using Container = std::list<Filter*>;

  Container filters{};
};

#endif