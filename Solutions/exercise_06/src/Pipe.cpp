#include "Pipe.h"
#include <functional>

using namespace std;

// We need to remove this overload for unique_ptr
// as they don't support copying; and therefore
// this function won't compile
//
// void Pipe::push(const Pipe::element_type& in_val)
// {
//     // Copy l-values
//     //
//     element = in_val;
// }

void Pipe::push(Pipe::element_type&& in_val)
{
  // in_val is an l-value expression
  // here, so must be moved again
  //
  element = move(in_val);
}

std::optional<Pipe::element_type> Pipe::pull()
{
  // Expire the internal
  // element by moving from
  // it.
  //
  if (element)
    return move(element);
  else
    return std::nullopt;
}
